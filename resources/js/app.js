/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.axios = require("axios");
/**
 * VUE JS
 */
window.Vue = require("vue");

import * as VueGoogleMaps from "vue2-google-maps";
import PhotoSwipe from "vue-photoswipe.js";
import "vue-photoswipe.js/dist/static/css/photoswipe.css";
import VueLazyload from "vue-lazyload";
import vSelect from "vue-select";
import VueFormWizard from "vue-form-wizard";
import "vue-form-wizard/dist/vue-form-wizard.min.css";
import Vue from "vue";
import Vuelidate from "vuelidate";

Vue.use(Vuelidate);
Vue.use(VueFormWizard);
Vue.component("v-select", vSelect);
Vue.use(VueLazyload, {
    preLoad: 1.3,

    loading: "/img/spacer.png",
    attempt: 1
});

Vue.use(VueGoogleMaps, {
    load: {
        key: process.env.MIX_GOOGLE_MAPS_API_KEY
        // This is required if you use the Autocomplete plugin
        // OR: libraries: 'places,drawing'
        // OR: libraries: 'places,drawing,visualization'
        // (as you require)

        //// If you want to set the version, you can do so:
        // v: '3.26',
    },

    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,

    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then set installComponents to 'false'.
    //// If you want to automatically install all the components this property must be set to 'true':
    installComponents: true
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    "homepage-home-search-component",
    require("./components/frontend/homepage/HomeSearchComponent.vue").default
);
Vue.component(
    "homepage-swiper-what-say-client",
    require("./components/frontend/homepage/SwiperWhatSaysClient.vue").default
);
Vue.component(
    "resultpage-results-map-listing",
    require("./components/frontend/resultpage/ResultsMapView.vue").default
);
Vue.component(
    "detailspage-sticky-sidebar-information",
    require("./components/frontend/detailspage/sidebarInformation.vue").default
);
Vue.component(
    "detailspage-swiper-carousel-images",
    require("./components/frontend/detailspage/SwipeCarrusel.vue").default
);
Vue.component(
    "profile-assets-menu",
    require("./components/frontend/profile/assets/menu.vue").default
);

// Dashboard
// En esta sección agregagaremos los componentes que se utilizarán en
// La sección administrativa o Dashboard
Vue.component(
    "dashboard-agency-commons-search-agency",
    require("./components/dashboard/commons/searchAgency.vue").default
);
Vue.component(
    "dashboard-user-datatable",
    require("./components/dashboard/users/UsersDatatable.vue").default
);
Vue.component(
    "dashboard-user-show-profile-card",
    require("./components/dashboard/profile/ProfileCard.vue").default
);
Vue.component(
    "dashboard-user-show-reset-password",
    require("./components/dashboard/profile/ResetPassword.vue").default
);
Vue.component(
    "dashboard-user-show-edit-information",
    require("./components/dashboard/profile/EditInfoAccount.vue").default
);
Vue.component(
    "dashboard-user-show-verification-user",
    require("./components/dashboard/profile/VerificationUser.vue").default
);
Vue.component(
    "dashboard-agency-create-form-wizard",
    require("./components/dashboard/agency/create.vue").default
);
Vue.component(
    "dashboard-agency-memberships-create",
    require("./components/dashboard/memberships/create.vue").default
);
Vue.component(
    "dashboard-agency-memberships-index",
    require("./components/dashboard/memberships/index.vue").default
);
Vue.component(
    "dashboard-agency-index-tab",
    require("./components/dashboard/agency/index.vue").default
);
Vue.component(
    "dashboard-agency-show-edit-info",
    require("./components/dashboard/agency/show/edit.vue").default
);

Vue.component(
    "dashboard-agency-admin-list-admin",
    require("./components/dashboard/agency/list/admin.vue").default
);
Vue.component(
    "dashboard-agency-admin-list-agent",
    require("./components/dashboard/agency/list/agent.vue").default
);
Vue.component(
    "dashboard-agency-properties-create",
    require("./components/dashboard/properties/create.vue").default
);
/**
 * Next, we will create a new filers used to format document
 */
Vue.filter("SetIdForm", function(id) {
    if (id < 10) {
        return "000" + id;
    } else if (id < 100) {
        return "00" + id;
    } else if (id < 1000) {
        return "0" + id;
    } else id;
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#main",
    created() {
        this.axiosConfig();
    },
    mounted() {
        this.setActiveMenu();
        this.verifySelectAgency();
    },
    data() {
        return {
            agencySelected: null,
            auth_token: null,
            dashboard_url: "/dashboard"
        };
    },
    methods: {
        axiosConfig() {
            var output = {};
            document.cookie.split(/\s*;\s*/).forEach(pair => {
                pair = pair.split(/\s*=\s*/);
                output[pair[0]] = pair.splice(1).join("=");
            });

            this.auth_token = output.auth_token;
            if (!this.auth_token) {
                this.openModal();
            }
            localStorage.setItem("req_token", output.auth_token);
            axios.defaults.headers.common["Authorization"] = output.auth_token
                ? "Bearer " +
                  decodeURIComponent(localStorage.getItem("req_token"))
                : "";
        },
        openModal() {
            let path = location.pathname;
            if (path.includes(this.dashboard_url)) {
                $("#defaultModal").modal("show");
                setTimeout(() => {
                    this.closeModal();
                }, 300000);
            }
        },
        closeModal() {
            $("#defaultModal").modal("hide");
            this.logout();
        },
        extendSession() {
            $("#defaultModal").modal("hide");
            document.getElementById("extendSessionForm").submit();
        },
        selectComplete(e) {
            this.agencySelected = e;
        },
        verifySelectAgency() {
            this.agencySelected =
                JSON.parse(localStorage.getItem("agency_selected")) ?? null;
        },
        logout() {
            localStorage.removeItem("agency_selected");
            document.getElementById("logout").submit();
        },
        setActiveMenu() {
            let item = document.getElementById("dashboard");
            let url = location.href;
            if (item) {
                item = item.getElementsByClassName("menu-item");
                for (let i of item) {
                    if (i.lastElementChild.href == url) {
                        if (i.parentElement.className == "ml-menu") {
                            i.parentElement.parentElement.className +=
                                " active open";
                            i.className += " active";
                        } else {
                            i.className += " active open";
                        }
                    }
                }
            }
        }
    }
});
