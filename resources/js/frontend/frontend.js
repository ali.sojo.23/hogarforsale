$(window).ready(function(e) {
    if ($("#content").length > 0) {
        function menuAdjustments() {
            var menu = $("#menu"),
                menuHeight = menu.outerHeight() + 15;

            if ($(window).width() > 767) {
                var scrollTop = $(this).scrollTop(),
                    topDistance = $("#content").offset().top;

                if (topDistance < scrollTop) {
                    if (
                        menu.hasClass("navbar-over") ||
                        menu.hasClass("absolute-top")
                    ) {
                        menu.addClass(
                            "fixed-top animated fadeInDown"
                        ).removeClass("absolute-top");
                    } else {
                        $("body")
                            .css({ "margin-top": menuHeight })
                            .find(menu)
                            .addClass("fixed-top animated fadeInDown");
                    }
                } else if (scrollTop == 0) {
                    if (menu.hasClass("navbar-over")) {
                        menu.addClass("absolute-top").removeClass(
                            "fixed-top animated fadeInDown"
                        );
                    } else {
                        $("body")
                            .css({ "margin-top": 0 })
                            .find(menu)
                            .removeClass("fixed-top animated fadeInDown");
                    }
                }
            }
        }
        $(window).on("scroll", function() {
            menuAdjustments();
        });
        $(document).resize(function() {
            var menu = $("#menu");
            if ($(document).width() < 768) {
                if (
                    menu.hasClass("navbar-over") ||
                    menu.hasClass("absolute-top")
                ) {
                    menu.addClass("absolute-top").removeClass(
                        "fixed-top animated fadeInDown"
                    );
                } else {
                    $("body")
                        .css({ "margin-top": 0 })
                        .find(menu)
                        .removeClass("fixed-top animated fadeInDown");
                }
            }
        });
    }
	let url = document.location.pathname;
	if(url === '/'){
		let menu = document.getElementById('menu').classList
		menu.add('navbar-over');
		menu.add('absolute-top');
	}
    $(document).scroll(function() {
        var btnTop = $("#to-top");
        if ($(document).scrollTop() >= 50) {
            btnTop.css({ visibility: "visible", opacity: "1" });
        } else {
            btnTop.css({ visibility: "hidden", opacity: "0" });
        }
    });

    $("#to-top").on("click", function() {
        $("html").animate({ scrollTop: 0 }, "slow");
    });

    $(".close-panel").on("click", function() {
        $.sidr("close", "sidebar");
    });
});
