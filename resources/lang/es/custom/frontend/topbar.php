<?php
    return [
        "hi" => "Hola",
        "buy" => [
            "buy" => "Comprar",
            "title" => "Casas en venta en " . env('APP_COUNTRY'),
            "home_for_sale" => "Casas en venta",
        ],
        "rent" => [
            "rent" => "Alquilar",
            "title" => "Casas en alquiler en " . env('APP_COUNTRY'),
            "home_for_sale" => "Casas en Alquiler",
        ],
        "agents" => [
            "agents" => "Agentes",
            "search_agent" => "¿Busca Profesionales?",
            "real_estate_agents" => "Agentes de bienes Raices",
            "property_managers" => "Administradores de propiedad",
            "be_agent" => "Soy un profesional",
            
        ],
    ];