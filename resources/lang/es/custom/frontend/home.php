<?php

return [
    "search" => [
        "title_one" => "Encuentra tu nuevo hogar",
        "title_two" => "Te ayudaremos a encontrar los mejores lugares para pasar el tiempo en cualquier ciudad del mundo.",
        "query_label" => "Ingrese una dirección, vecindario, ciudad, codigo postal",
        "search_button" => "Buscar",
        "for_rent" => "En renta",
        "for_sell" => "En venta"
    ]
];