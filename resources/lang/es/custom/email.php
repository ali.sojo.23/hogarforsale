<?php

return [
    "welcome" => [
        "title" => "Bienvenido",
        "message1" => "Se ha creado exitosamente su cuenta como: ",
        "message2" => "Para comenzar a usar el sistema avanzado de gestion de propiedades de Hogar For Sale debe dar click en el siguiente boton:",
        "button" => "Crear contraseña",
        "message3" => "Si eso no funciona, copie y pegue el siguiente enlace en su navegador:",

    ],
    "assigned" => [
        "title" => "Usuario Asignado",
        "message1" => "Su usuario se ha asignado exitosamente para administrar o gestionar una agencia en nuestro sistema",
        "message2" => "Para comenzar a usar el sistema avanzado de gestion de propiedades de Hogar For Sale debe dar click en el siguiente boton:",
        "button" => "IR AL DASHBOARD",
        "message3" => "Si eso no funciona, copie y pegue el siguiente enlace en su navegador:",

    ],
    "commons" => [
        "message1" => "Si tiene alguna pregunta, solo responda a este correo electrónico; estaremos encantados de ayudarlo.",
        "regards" => "Saludos",
        "hfs_team" => "El equipo de " . env('APP_NAME'),
    ],
];
