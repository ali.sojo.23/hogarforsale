<?php

return [
    "create" => [
        "dropzone" => [
            "title" => "¡Arrastra y suelta para subir contenido!",
            "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"
        ],
        "cancel" => "CAMBIAR FOTO"
    ],
    "index" => [],
    "show" => []
];
