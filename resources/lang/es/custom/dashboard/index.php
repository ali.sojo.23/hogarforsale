<?php

use Illuminate\Support\Facades\Auth;

return [
    "loader" => "Por favor espere ...",
    "title" => "Bienvenidos a Hogar For Sale - " . Auth::user()->firstname,
    "subtitle"  => "Es hora de aumentar las ventas de tus inmuebles",
    "breadcrumb" => [
        "index" => "Inicio"
    ],
    "admin" => "Administradores",
    "agents" => "Agentes",
    "properties" => "Propiedades",
    "modal" => [
        "extend_sessions" => "¿Extender sesión?",
        "extend_sessions_message" => "Su sesión está por concluir en los proximos 5 minutos, si desea continuar de click en continuar.",
        "button" => [
            "continue" => "CONTINUAR",
            "cancel" => "CANCELAR"
        ]
    ],
];
