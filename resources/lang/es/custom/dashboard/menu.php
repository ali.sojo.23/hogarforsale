<?php
return [
    "index" => "INICIO",
    "dashboard" => "Dashboard",
    "administration" => "ADMINISTRATION",
    "admin" => [
        "users" => "Usuarios",
        "users_admin" => "Usuarios Administrativos",
        "agencies" => "Agencias",
        "agency_index" => "Lista de agencias",
        "agency_create" => "Crear nueva agencias",
        "memberships" => "Membresias",
        "membership_index" => "Lista de membresias",
        "membership_create" => "Crear nueva membresias",
    ],
    "agency_admin" => "ADMINISTRACIÓN DE AGENCIA",
    "agency" => [
        "add_admin_to_agency" => "Asignar usuarios administrativos",
        "add_agent_to_agency" => "Asignar agentes"
    ],
    "admin_properties" => "ADMINISTRACIÓN DE PROPIEDADES",
    "properties" => [
        "create_new" => "Crear nueva propiedad",
        "list_by_ofert_type" => [
            "propreties_of_the_agency" => "Lista de Propiedades",
            "for_sale" => "En Venta",
            "for_rent" => "Arriendos/Alquileres"
        ]
    ],
    "right_sidebar" => [
        "template" => "Plantilla",
        "left_menu_template" => "Menu izquierdo",
        "languaje" => "Configurar idioma",
        "lang" => [
            "english" => "Ingles",
            "spanish" => "Español",
            "portuguese" => "Portugués"
        ],
        "light" => "Claro",
        "dark" => "Oscuro"
    ]

];
