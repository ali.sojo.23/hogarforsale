<?php

return [
    "breadcrumb" => [
        "memberships" => "Memebresias",
        "create" => "Crear Nueva"
    ],
    "title" => "Administración de membresias",
    "index" => [
        "subtitle" => "Lista de membresías disponibles para contratar",
        "datatable" => [
            "name" => "NOMBRE",
            "country" => "PAIS DE VENTA",
            "price" => "PRECIO",
            "properties" => "PROPIEDADES",
            "admins" => "ADMINISTRADORES",
            "agents" => "AGENTES"
        ],
    ],
    "create" => [
        "dropzone" => [
            "title" => "¡Arrastra y suelta para subir contenido!",
            "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"

        ],
        "title" => "Nombre de la mebresía",
        "price" => "Precio en USD",
        "description" => "Inserte un descripción para la membresía",
        "properties" => "Propiedades disponibles a crear por agencias, 0 es ilimitado",
        "admin_agency" => "Administradores disponibles a crear por agencias, 0 es ilimitado",
        "agency_agent" => "Agentes disponibles a crear por agencias, 0 es ilimitado",
        "country" => "Paìs donde funcionará la membresía, Vacio para todos",
        "messages" => [
            "required" => "Campo requerido",
            "minValue" => "El monto minimo especificado debe ser mayor de 0",
            "select_user" => "Debe seleccionar o crear un usuario",
            "complete_required" => "Debe completar todos los campos requeridos",
        ],
        "submit" => "Crear membresía"
    ]
];
