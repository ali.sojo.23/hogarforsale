<?php
return [
    "non_selected" => [
        "agency_title" => "Agencia",
        "agency_non_selected" => "Debe seleccionar primero una agencia desde la barra superior o haciendo <a class='d-inline' href='" . route('dashboard.index') . "' > click aquí </a>.",
        "agency_select" => "Seleccione una agencia ..."
    ],
    "breadcrumb" => [
        "agencies" => "Agencias",
        "create" => "Crear Nueva",
        "list" => "Agencias registradas",
        "set_admin" => "Asignar administrador",
        "set_agent" => "Asignar agentes",

    ],
    "title" => "Administración de Agencias",
    "index" => [
        "not_agency" => "No se ha agregado agencias",
        "subtitle" => "Lista de agencias inmobiliarias registradas.",
        "see_agency" => "Ver agencia"
    ],
    "show" => [
        "admin" => "Administradores",
        "agent" => "Agentes",
        "edit" => [
            "logo" => "Editar logo",
            "description" => "Editar descripción",
        ],
        "dropzone" => [
            "title" => "¡Arrastra y suelta para subir contenido!",
            "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"

        ],
        "message" => [
            "success" => [
                "updated" => "Agencia actualizado satisfactoriamente.",
            ],
        ]
    ],
    "list" => [
        "subtitle_admin" => "Asignar usuario administrador a las agencias disponibles",
        "subtitle_agent" => "Asignar agente a las agencias disponibles",
        "tab1" => [
            "create_user" => "¿Crear usuario?",
            "search_user" => "¿Buscar usuario?",
            "title" => "Administrador de la agencia",
            "firstname" => "Nombre",
            "lastname" => "Apellido",
            "img_profile" => "Imagen de Perfil",
            "complete_info" => "Completar los siguientes datos",
            "email" => "Correo electrónico",
            "search" => "Buscar",
            "create" => "Crear Usuario",
            "cancel" => "Cancelar"
        ],
        "user_created" => "Usuario creado y asignado a la agencia",
        "user_assigned" => "Usuario asignado a la agencia",
        "see_agency" => "Ver perfil",
        "user_found" => "Usuario existente"
    ],
    "create" => [
        "subtitle" => "Agregar nuevas agencias inmobiliarias",
        "formwizard" => [
            "title" => "Nueva Agencia inmobiliaria",
            "subtitle" => "COMPLETE EL SIGUIENTE FORMULARIO PARA AGREGAR NUEVAS AGENCIAS INMOBILIARIAS",
            "tab1" => [
                "create_user" => "¿Crear usuario?",
                "search_user" => "¿Buscar usuario?",
                "title" => "Administrador de la agencia",
                "firstname" => "Nombre",
                "lastname" => "Apellido",
                "img_profile" => "Imagen de Perfil",
                "complete_info" => "Completar los siguientes datos",
                "email" => "Correo electrónico",
                "search" => "Buscar",
                "cancel" => "Cancelar"
            ],
            "tab2" => [
                "title" => "Datos de la agencia",
                "subtitle" => "Completar Datos de la Agencia",
                "company_name" => "Razón Social",
                "comercial_name" => "Nombre Comercial",
                "rif" => "Número de identificación tributaria",
                "phone" => "Telefono",
                "mobile" => "Telefono Movil",
                "address" => "Escriba su codigo postal, o dirección",
                "country" => "País",
                "city" => "Ciudad",
                "postal_code" => "Código Postal",
            ],
            "tab3" => [
                "title" => "Asignación de membresias",
                "name" => "NOMBRE",
                "country" => "PAIS",
                "price" => "PRECIO",
                "properties" => "PROPIEDADES",
                "admins" => "ADMINISTRADORES",
                "agents" => "AGENTES",
                "selected" => "SELECIONADO",
                "unselected" => "SELECIONAR"
            ],
            "tab4" => [
                "title" => "Finalizar",
                "msg1" => "¡Se ha creado la agencia con exito!",
                "see_agency" => "VER AGENCIA"
            ],
            "button" => [
                "next" => "Siguiente",
                "back" => "Regresar",
                "done" => "Hecho"
            ],
            "dropzone" => [
                "title" => "¡Arrastra y suelta para subir contenido!",
                "subtitle" => "... o haga clic para seleccionar un archivo de su computadora"

            ],
            "messages" => [
                "required" => "Campo requerido",
                "email" => "Debe ingresar un campo valido de tipo email",
                "select_user" => "Debe seleccionar o crear un usuario",
                "complete_required" => "Debe completar todos los campos requeridos",
                "invalid_number" => "No es un número de telefono válido",
                "error" => [
                    "not_user" => "El usuario asignado a la cuenta de la agencia es incorreto, verifiquelo e intente nuevo."
                ]
            ]
        ]
    ],

];
