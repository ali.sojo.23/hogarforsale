<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "register_new_user" => "Registro de nuevo usuario",
    "register_new_acompa" => "Registro de nuevo cuidador",
    "firstname" => "Ingrese su nombre",
    "lastname" => "Ingrese su apellido",
    "terms_conditions" => "He leido y acepto los <a target='_blank' href='https://xn--acompao-9za.com/terminos-y-condiciones'>terminos y condiciones</a>",
    "already_account" => "¿Ya eres miembro?"

];
