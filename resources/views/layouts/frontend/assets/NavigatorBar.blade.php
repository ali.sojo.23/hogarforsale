<nav
    class="navbar navbar-expand-lg navbar-light"
    id="menu"
    style="background-image: none"
>
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{ asset('img/logo.png') }}" alt="" height="80" />
        </a>
        <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#menu-content"
            aria-controls="menu-content"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu-content">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item dropdown megamenu">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        @lang('custom/frontend/topbar.buy.buy')
                    </a>
                    <div class="dropdown-menu">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    @if(App::isLocale('es'))
                                                        @lang('custom/frontend/topbar.buy.title')
                                                    @endif
                                                </li>
                                                <li>
                                                <a href="{{route('sell.results')}}?{{env('APP_COUNTRY_URL')}}"
                                                        >@lang('custom/frontend/topbar.buy.home_for_sale')</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Property Listing
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing.html"
                                                        >List View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="property_grid.html"
                                                        >Grid View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing_map.html"
                                                        >Map View</a
                                                    >
                                                </li>

                                            </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown megamenu">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        @lang('custom/frontend/topbar.rent.rent')
                    </a>
                    <div class="dropdown-menu">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    @lang('custom/frontend/topbar.rent.title')
                                                </li>
                                                <li>
                                                <a href="{{route('rent.results')}}?{{env('APP_COUNTRY_URL')}}"
                                                        >@lang('custom/frontend/topbar.rent.home_for_sale')</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Property Listing
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing.html"
                                                        >List View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="property_grid.html"
                                                        >Grid View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing_map.html"
                                                        >Map View</a
                                                    >
                                                </li>

                                            </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item dropdown megamenu">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        @lang('custom/frontend/topbar.agents.agents')
                    </a>
                    <div class="dropdown-menu">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col col-md-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    @lang('custom/frontend/topbar.agents.search_agent')
                                                </li>
                                                <li>
                                                <a href="#"
                                                        >@lang('custom/frontend/topbar.agents.real_estate_agents')</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="#"
                                                            >@lang('custom/frontend/topbar.agents.property_managers')</a
                                                        >
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    @lang('custom/frontend/topbar.agents.be_agent')
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing.html"
                                                        >List View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="property_grid.html"
                                                        >Grid View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing_map.html"
                                                        >Map View</a
                                                    >
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                {{-- <li class="nav-item dropdown megamenu">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        Pages
                    </a>
                    <div class="dropdown-menu">
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col col-md-8">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-3">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Homepage
                                                </li>
                                                <li>
                                                    <a href="index.html"
                                                        >Homepage 1</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index2.html"
                                                        >Homepage 2</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index3.html"
                                                        >Homepage 3</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index4.html"
                                                        >Homepage 4</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index5.html"
                                                        >Homepage 5</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index6.html"
                                                        >Homepage 6</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="index7.html"
                                                        >Homepage 7</a
                                                    >
                                                </li>
                                                <li class="title">
                                                    Login Pages
                                                </li>
                                                <li>
                                                    <a href="signin.html"
                                                        >Signin</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="register.html"
                                                        >Register</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="forgot-password.html"
                                                        >Forgot Password</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Property Listing
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing.html"
                                                        >List View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="property_grid.html"
                                                        >Grid View</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_listing_map.html"
                                                        >Map View</a
                                                    >
                                                </li>
                                                <li class="title">
                                                    Single Property
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_single.html"
                                                        >Single View 1</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_single2.html"
                                                        >Single View 2</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="property_single3.html"
                                                        >Single View 3</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Other Pages
                                                </li>
                                                <li>
                                                    <a href="plans.html"
                                                        >Plans</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="information_page.html"
                                                        >Information Page</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="coming_soon.html"
                                                        >Coming Soon</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="404_error.html"
                                                        >Error Page</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="success.html"
                                                        >Success Page</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="contact.html"
                                                        >Contact Page</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="compare.html"
                                                        >Compare Properties</a
                                                    >
                                                </li>
                                                <li class="title">
                                                    Agent Pages
                                                </li>
                                                <li>
                                                    <a href="agent_list.html"
                                                        >Agent List</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="agent.html"
                                                        >Agent Profile</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6 col-lg-3">
                                            <ul class="list-unstyled">
                                                <li class="title">
                                                    Account Pages
                                                </li>
                                                <li>
                                                    <a
                                                        href="my_listing_add.html"
                                                        >Add Listing</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="my_bookmarked_listings.html"
                                                        >Bookmarked Listing</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_listings.html"
                                                        >My Listings</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_profile.html"
                                                        >My Profile</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_password.html"
                                                        >Change Password</a
                                                    >
                                                </li>
                                                <li>
                                                    <a
                                                        href="my_notifications.html"
                                                        >Notifications</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_membership.html"
                                                        >Membership</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_payments.html"
                                                        >Payments</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="my_account.html"
                                                        >Account</a
                                                    >
                                                </li>
                                                <li class="title">
                                                    Blog Pages
                                                </li>
                                                <li>
                                                    <a href="blog.html"
                                                        >Blog Archive</a
                                                    >
                                                </li>
                                                <li>
                                                    <a href="blog_single.html"
                                                        >Blog Single</a
                                                    >
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li> --}}
            </ul>

            <ul class="navbar-nav ml-auto">
                @auth
                @php
                    $role = Auth::user()->role;
                @endphp
                @if($role != 'admin' && $role != 'agency')
                <li class="nav-item dropdown user-account">
                    <a
                        class="nav-link dropdown-toggle"
                        href="#"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        @include('layouts.frontend.assets.user')
                    </a>
                    <div class="dropdown-menu">
                        <a
                            href="{{ route('profile.profile') }}"
                            class="dropdown-item"
                            >My Profile</a
                        >
                        <a
                            href="{{ route('profile.password') }}"
                            class="dropdown-item"
                            >Change Password</a
                        >
                        <a
                            href="{{ route('profile.notifications') }}"
                            class="dropdown-item"
                            >Notifications</a
                        >
                        <a
                            href="{{ route('profile.memberships') }}"
                            class="dropdown-item"
                            >Membership</a
                        >
                        <a href="my_payments.html" class="dropdown-item"
                            >Payments</a
                        >
                        <a
                            href="{{ route('dashboard.index') }}"
                            class="dropdown-item"
                            >Account</a
                        >
                    </div>
                </li>
                @else
                <li class="nav-item user-account">
                <a
                    class="nav-link "
                    href="{{ route('dashboard.index') }}"
                >
                    @include('layouts.frontend.assets.user')
                </a>
                </li>
                @endif
                @else
                <li class="nav-item">
                    <a
                        class="nav-link nav-btn"
                        href="{{ route('login') }}"
                        target="_blank"
                        ><span> {{ __("Login") }}</span></a
                    >
                </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
