<span
    class="user-image"
    style="background-image:url('{{ Auth::user()->feature_image ?? asset('img/logo.png') }}');"
></span>
{{ __("custom/frontend/topbar.hi") }}, {{Auth::user()->firstname}}
