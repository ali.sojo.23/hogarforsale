<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('img/logo.png')}}" type="image/png">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <link
            href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,200,300,400,500,700"
            rel="stylesheet"
        />

        <link href="{{asset('css/app.css?version='.env('APP_VERSION'))}}" rel="stylesheet" />
        @foreach ($styles as $style)
            <link rel="stylesheet" href="{{asset($style)}}">
        @endforeach
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <script async src="{{$googleApi ?? ''}}"></script> 
    
</head>
<body>
    <div id="main">
        @include('layouts.frontend.assets.NavigatorBar')
        @yield('content')
        <button class="btn btn-primary btn-circle" id="to-top">
            <i class="fa fa-angle-up"></i>
        </button>
        @include('layouts.frontend.assets.FooterBar')
    </div>
    @yield('scriptFooter')
    <script defer src="{{asset('js/app.js?version='.env('APP_VERSION'))}}"></script>  
</body>
</html>
