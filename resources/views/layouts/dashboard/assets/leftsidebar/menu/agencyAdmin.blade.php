<li class="header">{{__('custom/dashboard/menu.agency_admin')}}</li>
<li class="menu-item">
    <a href="{{route('dashboard.agency.list.admin')}}"
        ><i class="zmdi zmdi-account-o"></i><span>@lang('custom/dashboard/menu.agency.add_admin_to_agency')</span>
    </a>
</li>
<li class="menu-item">
    <a href="{{route('dashboard.agency.list.agent')}}"
        ><i class="zmdi zmdi-account"></i><span>@lang('custom/dashboard/menu.agency.add_agent_to_agency')</span>
    </a>
</li>
