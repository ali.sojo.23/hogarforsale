<li class="header">{{ __("custom/dashboard/menu.administration") }}</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-account"></i
        ><span>@lang('custom/dashboard/menu.admin.users')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.users') }}"
                >@lang('custom/dashboard/menu.admin.users_admin')</a
            >
        </li>
    </ul>
</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-home"></i
        ><span>@lang('custom/dashboard/menu.admin.agencies')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.index') }}"
                >@lang('custom/dashboard/menu.admin.agency_index')</a
            >
        </li>
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.create') }}"
                >@lang('custom/dashboard/menu.admin.agency_create')</a
            >
        </li>
    </ul>
</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-tag"></i>
        <span>@lang('custom/dashboard/menu.admin.memberships')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.memberships.index') }}"
                >@lang('custom/dashboard/menu.admin.membership_index')</a
            >
        </li>
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.memberships.create') }}"
                >@lang('custom/dashboard/menu.admin.membership_create')</a
            >
        </li>
    </ul>
</li>
