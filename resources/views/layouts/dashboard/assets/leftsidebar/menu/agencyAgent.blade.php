<li class="header">{{__('custom/dashboard/menu.admin_properties')}}</li>
<li class="menu-item">
    <a href="{{route('dashboard.agency.properties.create')}}"
        ><i class="zmdi zmdi-plus"></i><span>@lang('custom/dashboard/menu.properties.create_new')</span>
    </a>
</li>
<li class="menu-item-dropdown">
    <a href="javascript:void(0);" class="menu-toggle"
        ><i class="zmdi zmdi-home"></i
        ><span>@lang('custom/dashboard/menu.properties.list_by_ofert_type.propreties_of_the_agency')</span></a
    >
    <ul class="ml-menu">
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.properties.index','sell') }}"
                >@lang('custom/dashboard/menu.properties.list_by_ofert_type.for_sale')</a
            >
        </li>
        <li class="menu-item">
            <a href="{{ route('dashboard.agency.properties.index','rent') }}"
                >@lang('custom/dashboard/menu.properties.list_by_ofert_type.for_rent')</a
            >
        </li>
    </ul>
</li>
