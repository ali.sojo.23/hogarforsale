<div class="tab-pane stretchLeft" id="propertyInfo">
    <div  class="menu">
        <ul v-if="agencySelected" class="list">
            <li>
                <div class="user-info m-b-20 p-b-15">
                    <div class="image">
                        <a :href="'/dashboard/agency/show/'+agencySelected.id"
                            ><img class="bg-white" :src="agencySelected.logo" alt="User"
                        /></a>
                    </div>
                    <div class="detail">
                        <h4>@{{agencySelected.comercial_name}}</h4>
                        <small>@{{agencySelected.company_name}}</small>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <a v-if="agencySelected.facebook" title="facebook" :href="'https://facebook.com/'+agencySelected.facebook"><i class="zmdi zmdi-facebook"></i></a>
                            <a v-if="agencySelected.twitter" title="twitter" :href="'https://twitter.com/'+agencySelected.twitter"><i class="zmdi zmdi-twitter"></i></a>
                            <a v-if="agencySelected.instagram" title="instagram" :href="'https://instagram.com/'+agencySelected.instagram"><i class="zmdi zmdi-instagram"></i></a>
                        </div>
                        
                            <div class="col-6 p-r-0">
                                <h5 class="m-b-5">@{{agencySelected.admins.length}}</h5>
                                <small>Admins.</small>
                            </div>
                            <div class="col-6">
                                <h5 class="m-b-5">@{{agencySelected.agents.length}}</h5>
                                <small>Agents</small>
                            </div>
                            {{-- <div class="col-4 p-l-0">
                                <h5 class="m-b-5">148</h5>
                                <small>Clients</small>
                            </div>     --}}
                                                
                    </div>
                </div>
            </li>
            <li>
                <small class="text-muted">Location: </small>
                <p>@{{agencySelected.city}} @{{agencySelected.postal_code}}, @{{agencySelected.country}}</p>
                <hr>
                <small class="text-muted">Phone: </small>
                <p>
                    <a class="py-1" v-if="agencySelected.phone" href="tel:agencySelected.phone">@{{agencySelected.phone}}</a></p>
                <hr>
                <small class="text-muted">Mobile: </small>
                <p>
                    <a class="py-1" v-if="agencySelected.mobile" href="tel:agencySelected.mobile">@{{agencySelected.mobile}}</a>
                <hr>
                                    
            </li>                    
        </ul>
        <ul v-else class="list">
            <li>
                <div class="user-info m-b-20 p-b-15">
                    <div class="image">
                        <i class="zmdi zmdi-alert-octagon text-primary" style="font-size: 15ch;"></i>
                    </div>
                </div>
            </li>
            <li>
                <h4 class="text-center">
                    @lang('custom/dashboard/agency.non_selected.agency_non_selected')
                </h4>
            </li>
        </ul>
    </div>
</div>