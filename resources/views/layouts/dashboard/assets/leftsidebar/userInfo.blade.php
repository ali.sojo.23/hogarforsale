<div class="image">
    <a href="{{route('dashboard.profile')}}"
        ><img class="bg-white" src="{{Auth::user()->feature_image ?? '/img/logo.png'}}" alt="User"
    /></a>
</div>
<div class="detail">
    <h4>{{ucfirst(strtolower(Auth::user()->firstname))}} {{ucfirst(strtolower(Auth::user()->lastname))}}</h4>
    <small>{{ucfirst(strtolower(Auth::user()->role))}}</small>
</div>
