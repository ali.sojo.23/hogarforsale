<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-5 col-sm-12">
            <h2>{{$title??''}}
            <small>{{$subtitle ?? ''}}</small>
            </h2>
        </div>
        <div class="col-lg-5 col-md-7 col-sm-12">                
            
            <ul class="breadcrumb float-md-right">
                
                    
                
                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}"><i class="zmdi zmdi-home"></i></a></li>
                
                @foreach ($breadcrumb  as $item)
                    @if($loop->last)
                        <li class="breadcrumb-item active">{{$item}}</li>
                    @else
                        <li class="breadcrumb-item"><a href="javascript:void(0);">{{$item}}</a></li>
                    @endif
                @endforeach
                    
            </ul>                
        </div>
    </div>
</div>