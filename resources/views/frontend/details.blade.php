@extends('layouts.frontend.app') 
@section('content')
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col col-md-12 col-lg-12 col-xl-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Property for sale</a>
                </li>
                <li
                    class="breadcrumb-item active"
                    aria-current="page"
                >
                    2 bed semi-detached bungalow
                </li>
            </ol>
            <div class="page-header bordered mb0">
                <div class="row">
                    <div class="col-md-8">
                        {{-- <a href="#"  class="btn-return" title="Back"
                            ><i class="fa fa-angle-left"></i
                        ></a> --}}
                        <h1>
                            2 bed semi-detached bungalow
                            <span class="label label-bordered"
                                >For sale</span
                            >
                            <small
                                ><i class="fa fa-map-marker"></i>
                                Kirkstone Road, Middlesbrough
                                TS3</small
                            >
                        </h1>
                    </div>
                    <div class="col-md-4">
                        <div class="price">
                            £250,000 <small>$900/sq ft</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col col-md-12 col-lg-12 col-xl-10">
            <detailspage-swiper-carousel-images></detailspage-swiper-carousel-images>
        </div>
    </div>
</div>
<div id="content" class="item-single">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-md-12 col-lg-12 col-xl-10">
                <div
                    class="row row justify-content-md-center has-sidebar"
                >
                    <div class="col-md-7 col-lg-8">
                        <div>
                            <ul class="item-features">
                                <li><span>540</span> sq m</li>
                                <li><span>3</span> Rooms</li>
                                <li><span>1</span> Bedrooms</li>
                                <li><span>2</span> Bathrooms</li>
                            </ul>
                            <div class="item-description">
                                <h3 class="headline">
                                    Property description
                                </h3>
                                <p>
                                    <strong>Hall</strong>
                                    <strong>entrance</strong> UPVC
                                    double glazed door to the front,
                                    laminate flooring, storage
                                    cupboard, loft access and under
                                    floor heating.
                                </p>
                                <p>
                                    <strong
                                        >Lounge/diner/kitchen</strong
                                    >
                                    <strong
                                        >24' 6" x 16' 0" (7.47m x
                                        4.88m)</strong
                                    >
                                    Spacious L shape open plan
                                    living, UPVC double glazed
                                    window and bi-folding doors to
                                    the rear, laminate flooring,
                                    television and telephone
                                    connection points, power sockets
                                    and under floor heating.
                                </p>
                                <p>
                                    Fitted kitchen with wall and
                                    base cupboards, integrated Bosch
                                    electric hob and oven, cooker
                                    hood, lamona inset sink and
                                    drainer, mosaic style splash
                                    back tiling, integrated Bosch
                                    washing machine and dishwasher,
                                    integrated Bosch fridge freezer
                                    and power sockets.
                                </p>
                                <p>
                                    <strong>Bedroom</strong>
                                    <strong>one</strong>
                                    <strong
                                        >10' 08" x 10' 04" (3.25m x
                                        3.15m)</strong
                                    >
                                    UPVC double glazed window to the
                                    front, television connection
                                    point, power sockets and under
                                    floor heating.
                                </p>
                                <p>
                                    <strong>Bedroom</strong>
                                    <strong>two</strong>
                                    <strong
                                        >9' 05" x 9' 0" (2.87m x
                                        2.74m)</strong
                                    >
                                    UPVC double glazed window to the
                                    front, television connection
                                    point, power sockets and under
                                    floor heating.
                                </p>
                                <p>
                                    <strong>Bathroom</strong>
                                    <strong
                                        >8' 08" x 6' 08" (2.64m x
                                        2.03m)</strong
                                    >
                                    UPVC double glazed window to the
                                    side, tiled flooring, L shaped
                                    bath with fitted shower over
                                    head, wash hand basin, WC,
                                    extractor fan, tiled surrounds
                                    and under floor heating.
                                </p>
                                <p>
                                    <strong>Outside</strong>
                                    <strong>areas</strong> Front -
                                    large driveway with space for
                                    multiple vehicles.
                                </p>
                                <p>
                                    Rear - Large fully enclosed
                                    garden laid to lawn with patio
                                    around the property and side
                                    access.
                                </p>
                            </div>
                            <h3 class="headline">
                                Property Details
                            </h3>
                            <ul class="checked_list feature-list">
                                <li>
                                    <strong>Building Age:</strong> 2
                                    Years
                                </li>
                                <li>
                                    <strong>Parking:</strong>
                                    Attached Garage
                                </li>
                                <li>
                                    <strong>Cooling:</strong>
                                    Central Cooling
                                </li>
                                <li>
                                    <strong>Heating:</strong> Forced
                                    Air, Gas
                                </li>
                                <li>
                                    <strong>Sewer:</strong>
                                    Public/City
                                </li>
                                <li>
                                    <strong>Water:</strong> City
                                </li>
                            </ul>

                            <h3 class="headline">
                                Property Features
                            </h3>
                            <ul class="checked_list feature-list">
                                <li>Alarm</li>
                                <li>Gym</li>
                                <li>Internet</li>
                                <li>Swimming Pool</li>
                                <li>Window Covering</li>
                            </ul>

                            <div class="item-navigation">
                                <ul
                                    class="nav nav-tabs v2"
                                    role="tablist"
                                >
                                    <li role="presentation">
                                        <a
                                            href="#map"
                                            aria-controls="map"
                                            role="tab"
                                            data-toggle="tab"
                                            class="active"
                                            ><i
                                                class="fa fa-map"
                                            ></i>
                                            <span class="hidden-xs"
                                                >Map &amp;
                                                nearby</span
                                            ></a
                                        >
                                    </li>
                                    <li role="presentation">
                                        <a
                                            href="#streetview"
                                            aria-controls="streetview"
                                            role="tab"
                                            data-toggle="tab"
                                            ><i
                                                class="fa fa-road"
                                            ></i>
                                            <span class="hidden-xs"
                                                >Street View</span
                                            ></a
                                        >
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div
                                        role="tabpanel"
                                        class="tab-pane active"
                                        id="map"
                                    >
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1215.7401235613713!2d1.4497354260471211!3d52.45232942952154!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d9f169c5a088db%3A0x75a6abde48cc5adc!2sKents+Ln%2C+Bungay+NR35+1JF%2C+UK!5e0!3m2!1sen!2sin!4v1489862715790"
                                            width="600"
                                            height="450"
                                            style="border:0;"
                                            allowfullscreen
                                        ></iframe>
                                    </div>
                                    <div
                                        role="tabpanel"
                                        class="tab-pane"
                                        id="streetview"
                                    >
                                        <iframe
                                            src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sen!2s!4v1489861898447!6m8!1m7!1sGz9bS-GXSJE28jHD19m7KQ!2m2!1d52.45191646727986!2d1.451480542718656!3f0!4f0!5f0.8160813932612223"
                                            width="600"
                                            height="450"
                                            style="border:0"
                                            allowfullscreen
                                        ></iframe>
                                    </div>
                                </div>
                            </div>

                            <div class="item-attachments">
                                <h3 class="headline">
                                    Download Documents
                                </h3>
                                <a
                                    href="#"
                                    class="btn btn-lg btn-inverse"
                                    ><i
                                        class="fa fa-cloud-download"
                                        aria-hidden="true"
                                    ></i>
                                    Brochure</a
                                >
                                <a
                                    href="#"
                                    class="btn btn-lg btn-inverse"
                                    ><i
                                        class="fa fa-cloud-download"
                                        aria-hidden="true"
                                    ></i>
                                    Floor Plan</a
                                >
                                <a
                                    href="#"
                                    class="btn  btn-lg btn-inverse"
                                    ><i
                                        class="fa fa-cloud-download"
                                        aria-hidden="true"
                                    ></i>
                                    Layout Plan</a
                                >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-4">
                        <detailspage-sticky-sidebar-information></detailspage-sticky-sidebar-information>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Lead Form Modal -->
<div
    class="modal fade  item-badge-rightm"
    id="leadform"
    tabindex="-1"
    role="dialog"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="media">
                    <div class="media-left">
                        <img
                            src="{{asset('img/demo/property/thumb/1.jpg')}}"
                            width="60"
                            class="img-rounded mt5"
                            alt=""
                        />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Request details for 2 bed semi-detached
                            bungalow for sale
                        </h4>
                        Kents Lane, Bungay NR35
                    </div>
                </div>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Your Name</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Your Name"
                        />
                    </div>
                    <div class="form-group">
                        <label>Your Email</label>
                        <input
                            type="email"
                            class="form-control"
                            placeholder="Your Email"
                        />
                    </div>
                    <div class="form-group">
                        <label>Your Telephone</label>
                        <input
                            type="tel"
                            class="form-control"
                            placeholder="Your Telephone"
                        />
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea
                            rows="4"
                            class="form-control"
                            placeholder="Please include any useful details, i.e. current status, availability for viewings, etc."
                        ></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-link"
                    data-dismiss="modal"
                >
                    Cancel
                </button>
                <button type="button" class="btn btn-primary">
                    Request Details
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Email to friend Modal -->
<div
    class="modal fade item-badge-rightm"
    id="email-to-friend"
    tabindex="-1"
    role="dialog"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="media">
                    <div class="media-left">
                        <img
                            src="{{asset('img/demo/property/thumb/1.jpg')}}"
                            width="60"
                            class="img-rounded mt5"
                            alt=""
                        />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Email friend about 2 bed semi-detached
                            bungalow for sale
                        </h4>
                        Kents Lane, Bungay NR35
                    </div>
                </div>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Your Name</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Your Name"
                        />
                    </div>
                    <div class="form-group">
                        <label>Your Email</label>
                        <input
                            type="email"
                            class="form-control"
                            placeholder="Your Email"
                        />
                    </div>
                    <div class="form-group">
                        <label>Friends Email</label>
                        <input
                            type="email"
                            class="form-control"
                            placeholder="Friends Email"
                        />
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <textarea
                            rows="4"
                            class="form-control"
                            placeholder=""
                        >
I thought you might want to take a look at this property for sale.</textarea
                        >
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-link"
                    data-dismiss="modal"
                >
                    Cancel
                </button>
                <button type="button" class="btn btn-primary">
                    Request Details
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Report Listing Modal -->
<div
    class="modal fade item-badge-rightm"
    id="report-listing"
    tabindex="-1"
    role="dialog"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="media">
                    <div class="media-left">
                        <i
                            class="fa fa-3x fa-exclamation-circle"
                            aria-hidden="true"
                        ></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                            Report Listing for 2 bed semi-detached
                            bungalow for sale
                        </h4>
                        Kents Lane, Bungay NR35
                    </div>
                </div>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label>Contact Name</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Contact Name"
                        />
                    </div>
                    <div class="form-group">
                        <label>Email Address</label>
                        <input
                            type="email"
                            class="form-control"
                            placeholder="Email Address"
                        />
                    </div>
                    <div class="form-group">
                        <label>Nature of report</label>
                        <select class="form-control">
                            <option value="">Please Select</option>
                            <option value="no_longer_available"
                                >Property is no longer
                                available</option
                            >
                            <option value="incorrect_price"
                                >Price listed is incorrect</option
                            >
                            <option
                                value="incorrect_last_sold_price"
                                >Last sold price incorrect</option
                            >
                            <option value="incorrect_description"
                                >Property description is
                                inaccurate</option
                            >
                            <option value="incorrect_location"
                                >Property location is
                                incorrect</option
                            >
                            <option value="incorrect_content"
                                >Problem with photos, floorplans,
                                etc.</option
                            >
                            <option value="inappropriate_video"
                                >Problem with the video</option
                            >
                            <option value="agent_not_contactable"
                                >Agent is not contactable</option
                            >
                            <option value="incorrect_running_costs"
                                >Running costs is displaying
                                inaccurate values</option
                            >
                            <option value="other"
                                >Other (please specify)</option
                            >
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Description of content issue </label>
                        <textarea
                            rows="4"
                            class="form-control"
                            placeholder="Please provide as much information as possible..."
                        ></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button
                    type="button"
                    class="btn btn-link"
                    data-dismiss="modal"
                >
                    Cancel
                </button>
                <button type="button" class="btn btn-primary">
                    Report Error
                </button>
            </div>
        </div>
    </div>
</div>
@endsection