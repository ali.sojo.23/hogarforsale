@extends('layouts.frontend.app') 
@section('content')

<div class="clearfix"></div>
<div id="content">
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-lg-12 col-xl-10">
                <div class="row has-sidebar">
                    @include('frontend.profile.assets.menu')
                    <div class="col-md-7 col-lg-8 col-xl-8">
                        <div class="page-header bordered">
                            <h1>Change Password</h1>
                        </div>
                        <form action="index.php">
                            <div class="form-group">
                                <label>Your current password</label>
                                <input
                                    type="text"
                                    class="form-control form-control-lg"
                                    placeholder="Your current password"
                                    autofocus
                                />
                            </div>
                            <div class="form-group">
                                <label>Your new password</label>
                                <input
                                    type="text"
                                    class="form-control form-control-lg"
                                    placeholder="Your new password"
                                />
                            </div>
                            <div class="form-group">
                                <label>Repeat new password</label>
                                <input
                                    type="text"
                                    class="form-control form-control-lg"
                                    placeholder="Repeat new password"
                                />
                            </div>
                            <hr />
                            <div class="form-group action">
                                <button
                                    type="submit"
                                    class="btn btn-lg btn-primary"
                                >
                                    Update Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
