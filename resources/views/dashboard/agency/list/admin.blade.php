@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/agency.list')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                
                <div class="card">
                    <div class="body">
                        @empty($agency)
                        <ul class="nav nav-tabs padding-0">
                            
                            
                                <span class="mx-auto text-center">
                                    <i class="zmdi zmdi-alert-octagon text-primary" style="font-size: 32px"></i> 
                                    <br>
                                     @lang('custom/dashboard/agency.index.not_agency')
                                </span> 
                            
                        </ul>
                        @else
                        <div class="tab-content m-t-10">
                    
                            <div class="tab-pane active">
                                <dashboard-agency-admin-list-admin :t="'{{Cookie::get('template')}}'" :a="{{$agency}}" :lang="{{$lang}}" v-on:select-agency="selectComplete"></dashboard-agency-admin-list-admin>
                            </div>
                            
                                
                        </div>
                        @endempty
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>

@endsection