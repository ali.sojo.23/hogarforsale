@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/agency.index')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
                        <ul class="nav nav-tabs padding-0">
                            @forelse ($agency as $key => $value)
                                <li class="nav-item"><a class="nav-link @if($loop->first)active @endif" data-toggle="tab" href="#{{$key}}">{{$key}}</a></li>
                            @empty
                                <span class="mx-auto text-center">
                                    <i class="zmdi zmdi-alert-octagon text-primary" style="font-size: 32px"></i> 
                                    <br>
                                     @lang('custom/dashboard/agency.index.not_agency')
                                </span>
                            @endforelse
                        </ul>
                    </div>
                </div>
                <div class="tab-content m-t-10">
                    @forelse ($agency as $key => $value)
                    <div class="tab-pane @if($loop->first)active @endif" id="{{$key}}">
                        <dashboard-agency-index-tab :t="'{{Cookie::get('template')}}'" :lang="{{$lang}}" :country="'{{$key}}'"></dashboard-agency-index-tab>
                    </div>
                    @empty
                        
                    @endforelse
                </div>
            </div>
        </div>
    </div>

@endsection