@extends('layouts.dashboard.app') 
@php 
    $lang = json_encode(__('custom/dashboard/agency.show')); 
@endphp 
@section('content')

<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-4 col-md-12">
            <div class="card profile-header">
                 <div class="body text-center">
                    <div class="profile-image">
                        <img
                            src="{{$agency->logo ?? '/img/logo.png'}}"
                            alt=""
                        />
                    </div>
                    <div>
                        <h4 class="m-b-0">{{$agency->comercial_name}}</h4>
                        <span class="job_post">{{$agency->company_name}}</span>
                        <p>
                            {{$agency->city}} {{$agency->postal_code}},
                            {{$agency->country}}
                        </p>
                    </div>

                    <p class="social-icon m-t-5 m-b-0">
                        @if ($agency->twitter)
                        <a
                            class="mx-2"
                            title="Twitter"
                            href="https://twitter.com/{{$agency->twitter}}"
                            ><i class="zmdi zmdi-twitter"></i
                        ></a>
                        @endif @if ($agency->facebook)
                        <a
                            class="mx-2"
                            title="Facebook"
                            href="https://facebook.com/{{$agency->facebook}}"
                            ><i class="zmdi zmdi-facebook"></i
                        ></a>
                        @endif @if ($agency->linkedin)
                        <a
                            class="mx-2"
                            title="linkedin"
                            href="https://inkedin.com/in/{{$agency->linkedin}}"
                            ><i class="zmdi zmdi-linkedin"></i
                        ></a>
                        @endif @if ($agency->instagram)
                        <a
                            class="mx-2"
                            title="Instagram"
                            href="https://instagram.com/{{$agency->instagram}}"
                            ><i class="zmdi zmdi-instagram"></i
                        ></a>
                        @endif
                    </p>
                </div>
            </div>
            <div class="card">
                <div class="body">
                    <div class="workingtime">
                        <h6>Working Time</h6>
                        <small class="text-muted">Tuesday</small>
                        <p>06:00 AM - 07:00 AM</p>
                        <hr />
                        <small class="text-muted">Thursday</small>
                        <p>06:00 AM - 07:00 AM</p>
                        <hr />
                    </div>
                    <div class="reviews">
                        <h6>Reviews</h6>
                        <small class="text-muted">Staff</small>
                        <p>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                        </p>
                        <hr />
                        <small class="text-muted">Punctuality</small>
                        <p>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                        </p>
                        <hr />
                        <small class="text-muted">Helpfulness</small>
                        <p>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                        </p>
                        <hr />
                        <small class="text-muted">Knowledge</small>
                        <p>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                        </p>
                        <hr />
                        <small class="text-muted">Cost</small>
                        <p>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                            <i class="zmdi zmdi-star-outline"></i>
                        </p>
                        <hr />
                    </div>
                </div>
            </div>
            <div class="card">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a
                            class="nav-link active"
                            data-toggle="tab"
                            href="#adminTAB"
                            >@lang('custom/dashboard/agency.show.admin')</a
                        >
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#agentTAB"
                            >@lang('custom/dashboard/agency.show.agent')</a
                        >
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane body active" id="adminTAB">
                        <ul class="right_chat list-unstyled">
                            @forelse ($agency->admins as $item)
                            <li class="online">
                                <a
                                    href="{{route('dashboard.users.show',$item->user->id)}}"
                                >
                                    <div class="media">
                                        <img
                                            class="media-object "
                                            src="{{$item->user->feature_image ?? '/img/logo.png'}}"
                                            alt=""
                                        />
                                        <div class="media-body">
                                            <span class="name"
                                                >{{$item->user->firstname}}
                                                {{$item->user->lastname}}</span
                                            >
                                            <span class="message"
                                                >{{$item->user->info->country ?? ''
                                                }},
                                                {{$item->user->info->ciudad ?? ''}}</span
                                            >
                                            <span
                                                class="badge badge-outline status"
                                            ></span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @empty @endforelse
                        </ul>
                    </div>
                    <div class="tab-pane body" id="agentTAB">
                        <ul class="new_friend_list list-unstyled row">
                            @forelse ($agency->agents as $item)
                            <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                <a
                                    href="{{route('dashboard.users.show',$item->user->id)}}"
                                >
                                    <img
                                        src="{{$item->user->feature_image ?? '/img/logo.png'}}"
                                        class="img-thumbnail"
                                        alt="User Image"
                                    />
                                    <h6 class="users_name">
                                        {{$item->user->firstname}}
                                    </h6>
                                    <small
                                        class="join_date"
                                        >{{$item->user->lastname}}</small
                                    >
                                </a>
                            </li>

                            @empty @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-12">
            <div class="card">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a
                            class="nav-link active"
                            data-toggle="tab"
                            href="#about"
                            >About</a
                        >
                    </li>
                    @can('agency.admin')
                     @if (Auth::user()->hasRole('admin')|| Auth::user()->isAgencyAdmin($agency->id))
                     <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#Account"
                            >Account</a
                        >
                    </li>
                     @endif
                    @endcan
                </ul>
                <div class="tab-content">
                    <div class="tab-pane body active" id="about">
                        <p>{{$agency->description}}</p>
                        <h6>Qualifications</h6>
                        <hr />
                        <ul class="list-unstyled">
                            <li>
                                <p>
                                    <strong>Hospital Affiliations:</strong> UCSF
                                    MEDICAL CENTER
                                </p>
                            </li>
                            <li>
                                <p>
                                    <strong>Medical School:</strong> Palmer
                                    College of Chiropractic 1978
                                </p>
                            </li>
                            <li>
                                <p><strong>Residency:</strong> New york</p>
                            </li>
                            <li>
                                <p>
                                    <strong>Certifications:</strong> Certified
                                    Chiropractic Sports Physician 1982
                                </p>
                            </li>
                            <li>
                                <p><strong>Gender:</strong> Female</p>
                            </li>
                            <li>
                                <p>
                                    <strong>Experience / Tranining:</strong>
                                    Past-President, Int. Fed. 1991
                                </p>
                            </li>
                            <li>
                                <p>
                                    <strong>Internship:</strong> Palmer Clinic
                                </p>
                            </li>
                        </ul>
                        <h6>Specialties</h6>
                        <hr />
                        <ul class="list-unstyled specialties">
                            <li>Breast Surgery</li>
                            <li>Colorectal Surgery</li>
                            <li>Endocrinology</li>
                            <li>Cardiology</li>
                            <li>Cosmetic Dermatology</li>
                            <li>Mole checks and monitoring</li>
                            <li>Clinical Neurophysiology</li>
                        </ul>
                    </div>
                    @can('agency.admin')
                    @if (Auth::user()->hasRole('admin')|| Auth::user()->isAgencyAdmin($agency->id))
                    <div class="tab-pane body" id="Account">
                        <dashboard-agency-show-edit-info
                            :a="{{ $agency }}"
                            :lang="{{ $lang }}"
                        ></dashboard-agency-show-edit-info>
                    </div>
                    @endif
                    @endcan
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Recent</strong> Activity</h2>
                    <ul class="header-dropdown">
                        <li class="remove">
                            <a role="button" class="boxs-close"
                                ><i class="zmdi zmdi-close"></i
                            ></a>
                        </li>
                    </ul>
                </div>
                {{-- <div class="body user_activity">
                    <div class="streamline b-accent">
                        <div class="sl-item">
                            <img
                                class="user rounded-circle"
                                src="..\assets\images\xs\avatar4.jpg"
                                alt=""
                            />
                            <div class="sl-content">
                                <h5 class="m-b-0">Admin Birthday</h5>
                                <small
                                    >Jan 21
                                    <a
                                        href="javascript:void(0);"
                                        class="text-info"
                                        >Sophia</a
                                    >.</small
                                >
                            </div>
                        </div>
                        <div class="sl-item">
                            <img
                                class="user rounded-circle"
                                src="..\assets\images\xs\avatar5.jpg"
                                alt=""
                            />
                            <div class="sl-content">
                                <h5 class="m-b-0">Add New Contact</h5>
                                <small
                                    >30min ago
                                    <a href="javascript:void(0);">Alexander</a
                                    >.</small
                                >
                                <small><strong>P:</strong> +264-625-2323</small>
                                <small
                                    ><strong>E:</strong>
                                    maryamamiri@gmail.com</small
                                >
                            </div>
                        </div>
                        <div class="sl-item">
                            <img
                                class="user rounded-circle"
                                src="..\assets\images\xs\avatar6.jpg"
                                alt=""
                            />
                            <div class="sl-content">
                                <h5 class="m-b-0">General Surgery</h5>
                                <small
                                    >Today
                                    <a href="javascript:void(0);">Grayson</a
                                    >.</small
                                >
                                <small
                                    >The standard chunk of Lorem Ipsum used
                                    since the 1500s is reproduced</small
                                >
                            </div>
                        </div>
                        <div class="sl-item">
                            <img
                                class="user rounded-circle"
                                src="..\assets\images\xs\avatar7.jpg"
                                alt=""
                            />
                            <div class="sl-content">
                                <h5 class="m-b-0">General Surgery</h5>
                                <small
                                    >45min ago
                                    <a
                                        href="javascript:void(0);"
                                        class="text-info"
                                        >Fidel Tonn</a
                                    >.</small
                                >
                                <small><strong>P:</strong> +264-625-2323</small>
                                <small
                                    >The standard chunk of Lorem Ipsum used
                                    since the 1500s is reproduced used since the
                                    1500s is reproduced</small
                                >
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>

@endsection
