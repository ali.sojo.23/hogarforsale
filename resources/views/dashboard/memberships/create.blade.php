@extends('layouts.dashboard.app')
@php
    $lang = json_encode(__('custom/dashboard/memberships.create'));
@endphp
@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
                <dashboard-agency-memberships-create :lang="{{$lang}}"></dashboard-agency-memberships-create>
            </div>
        </div>
    </div>
</div>
@endsection