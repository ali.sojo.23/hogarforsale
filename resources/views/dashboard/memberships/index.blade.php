@extends('layouts.dashboard.app')
@php
    $lang = json_encode(__('custom/dashboard/memberships.index.datatable'));
@endphp
@section('content')
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card patients-list">
                
                <div class="body">
                    
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs padding-0">
                        @foreach ($memberships as $m)
                            <li  class="nav-item">
                                <a class="nav-link @if ($loop->first) active @endif" data-toggle="tab" href="#membership{{str_replace('.','_',$m)}}">
                                    {{$m}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                        
                    <!-- Tab panes -->
                    <div class="tab-content m-t-10">
                        @foreach ($memberships as $m)
                            <dashboard-agency-memberships-index 
                                @if ($loop->first)
                                    :active="true"
                                @else
                                    :active="false"
                                @endif                          
                                :type="'{{$m}}'"
                                :lang="{{$lang}}"
                                >
                            </dashboard-agency-memberships-index>                                
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection