@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/users.datatable')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                    
                    <div class="body">
                        
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs padding-0">
                            @foreach ($users as $u)
                                <li  class="nav-item">
                                    <a class="nav-link @if ($loop->first) active @endif" data-toggle="tab" href="#user{{str_replace('.','_',$u)}}">
                                        {{$u}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            @foreach ($users as $user)
                                <dashboard-user-datatable  
                                    @if ($loop->first)
                                        :active="true"
                                    @else
                                        :active="false"
                                    @endif
                                    
                                    :clave="'{{$user}}'"
                                    :lang="{{$lang}}">
                                </dashboard-user-datatable>                                
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection