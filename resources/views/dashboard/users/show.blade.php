@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('custom/dashboard/users.profile')); 
@endphp
@section('content')

    
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <dashboard-user-show-profile-card
                    :users="{{ $user }}"
                    :trans="{{ $lang }}"
                ></dashboard-user-show-profile-card>
                {{-- <div class="card">
                    <div class="body">
                        <div class="workingtime">
                            <h6>Working Time</h6>
                            <small class="text-muted">Tuesday</small>
                            <p>06:00 AM - 07:00 AM</p>
                            <hr />
                            <small class="text-muted">Thursday</small>
                            <p>06:00 AM - 07:00 AM</p>
                            <hr />
                        </div>
                        <div class="reviews">
                            <h6>Reviews</h6>
                            <small class="text-muted">Staff</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Punctuality</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Helpfulness</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Knowledge</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Cost</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                        </div>
                    </div>
                </div>
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="tab"
                                href="#Followers"
                                >Followers</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#friends"
                                >Friends</a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane body active" id="Followers">
                            <ul class="right_chat list-unstyled">
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar4.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Chris Fox</span
                                                >
                                                <span class="message"
                                                    >Designer, Blogger</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar5.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Joge Lucky</span
                                                >
                                                <span class="message"
                                                    >Java Developer</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar2.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Isabella</span
                                                >
                                                <span class="message"
                                                    >CEO, Thememakker</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar1.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Folisise Chosielie</span
                                                >
                                                <span class="message"
                                                    >Art director, Movie
                                                    Cut</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar3.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Alexander</span
                                                >
                                                <span class="message"
                                                    >Writter, Mag Editor</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane body" id="friends">
                            <ul class="new_friend_list list-unstyled row">
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar1.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Jackson</h6>
                                        <small class="join_date">Today</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar2.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Aubrey</h6>
                                        <small class="join_date"
                                            >Yesterday</small
                                        >
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar3.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Oliver</h6>
                                        <small class="join_date">08 Nov</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar4.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Isabella</h6>
                                        <small class="join_date">12 Dec</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar1.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Jacob</h6>
                                        <small class="join_date">12 Dec</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar5.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Matthew</h6>
                                        <small class="join_date">17 Dec</small>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="tab"
                                href="#about"
                        >{{ __('custom/dashboard/users.profile.print_info.about')}} {{$user->firstname}}</a
                            >
                        </li>
                        @if (Auth::user()->role == 'admin' || Auth::user()->id == $user->id)
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#Account"
                                >{{ __('custom/dashboard/users.profile.print_info.account')}}</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#verifyAccount"
                                >{{ __('custom/dashboard/users.profile.print_info.verify')}}</a
                            >
                        </li>
                        @endif
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane body active" id="about">
                            <p>
                              {!!  $user->info->description ?? ''  !!} 
                            </p>
                            @if ($user->info)
                            <h6>{{ __('custom/dashboard/users.profile.print_info.general_info.title')}}</h6>
                            <hr />
                            <ul class="list-unstyled">
                                <li>
                                    <p><strong>{{ __('custom/dashboard/users.profile.print_info.general_info.gender')}}</strong> 
                                        @if($user->info->gender == 0) 
                                        {{ __('custom/dashboard/users.profile.edit_info.gender.male')}}
                                        @elseif($user->info->gender == 1) 
                                        {{ __('custom/dashboard/users.profile.edit_info.gender.female')}}
                                        @elseif($user->info->gender == 2) 
                                        {{ __('custom/dashboard/users.profile.edit_info.gender.no_binary')}}
                                        @endif
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('custom/dashboard/users.profile.print_info.general_info.birthdate')}}</strong> 
                                        {{ $user->info->birthdate }}
                                    </p>
                                </li>
                            </ul>
                            @endif
                            {{-- <h6>Specialties</h6>
                            <hr />
                            <ul class="list-unstyled specialties">
                                <li>Breast Surgery</li>
                                <li>Colorectal Surgery</li>
                                <li>Endocrinology</li>
                                <li>Cardiology</li>
                                <li>Cosmetic Dermatology</li>
                                <li>Mole checks and monitoring</li>
                                <li>Clinical Neurophysiology</li>
                            </ul> --}}
                        </div>
                        <div class="tab-pane body" id="Account">
                            <dashboard-user-show-reset-password
                                :users="{{ $user }}"
                                :trans="{{ $lang }}"
                                :csrf="'{{ Session::token() }}'"
                            ></dashboard-user-show-reset-password>
                            <hr />
                            <dashboard-user-show-edit-information
                                :users="{{ $user }}"
                                :trans="{{ $lang }}"
                            ></dashboard-user-show-edit-information>
                        </div>
                        <div id="verifyAccount" class="tab-pane body">
                            <dashboard-user-show-verification-user
                                :auth="{{Auth::user()}}"
                                :user="{{$user}}"
                                :tkn="'{{Session::token()}}'"
                                :lang="{{$lang}}"
                            ></dashboard-user-show-verification-user>
                        </div>
                    </div>
                </div>
                {{-- <div class="card">
                    <div class="header">
                        <h2><strong>Recent</strong> Activity</h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"
                                    ><i class="zmdi zmdi-close"></i
                                ></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body user_activity">
                        <div class="streamline b-accent">
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar4.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">Admin Birthday</h5>
                                    <small
                                        >Jan 21
                                        <a
                                            href="javascript:void(0);"
                                            class="text-info"
                                            >Sophia</a
                                        >.</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar5.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">Add New Contact</h5>
                                    <small
                                        >30min ago
                                        <a href="javascript:void(0);"
                                            >Alexander</a
                                        >.</small
                                    >
                                    <small
                                        ><strong>P:</strong>
                                        +264-625-2323</small
                                    >
                                    <small
                                        ><strong>E:</strong>
                                        maryamamiri@gmail.com</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar6.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">General Surgery</h5>
                                    <small
                                        >Today
                                        <a href="javascript:void(0);">Grayson</a
                                        >.</small
                                    >
                                    <small
                                        >The standard chunk of Lorem Ipsum used
                                        since the 1500s is reproduced</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar7.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">General Surgery</h5>
                                    <small
                                        >45min ago
                                        <a
                                            href="javascript:void(0);"
                                            class="text-info"
                                            >Fidel Tonn</a
                                        >.</small
                                    >
                                    <small
                                        ><strong>P:</strong>
                                        +264-625-2323</small
                                    >
                                    <small
                                        >The standard chunk of Lorem Ipsum used
                                        since the 1500s is reproduced used since
                                        the 1500s is reproduced</small
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>


@endsection
