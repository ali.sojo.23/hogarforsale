@extends('layouts.dashboard.app') 
@section('content')
<div v-if="agencySelected" class="container-fluid">
<div v-if="agencySelected.membership" class="row clearfix">
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="body">
                <h3 class="number count-to m-b-0" data-from="0" :data-to="agencySelected.admins.length" data-speed="1000" data-fresh-interval="2"> @{{agencySelected.admins.length}} <i class="zmdi zmdi-trending-up float-right"></i></h3>
                <p class="text-muted">@lang('custom/dashboard/index.admin')</p>
                <div class="progress">
                    <div class="progress-bar l-blush" role="progressbar" :aria-valuenow="agencySelected.admins.length" aria-valuemin="0" :aria-valuemax="agencySelected.membership.admin_agency" :style="'width: '+agencySelected.admins.length*100/agencySelected.membership.admin_agency+'%;'"></div>
                </div>
                <small>Total: @{{agencySelected.membership.admin_agency}}</small>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="body">
                <h3 class="number count-to m-b-0" data-from="0" :data-to="agencySelected.agents.length" data-speed="1000" data-fresh-interval="2">@{{agencySelected.agents.length}} <i class="zmdi zmdi-trending-up float-right"></i></h3>
                <p class="text-muted">@lang('custom/dashboard/index.agents')</p>
                <div class="progress">
                    <div class="progress-bar l-green" role="progressbar" :aria-valuenow="agencySelected.agents.length" aria-valuemin="0" :aria-valuemax="agencySelected.membership.agency_agent" :style="'width: '+agencySelected.agents.length*100/agencySelected.membership.agency_agent+'%;'"></div>
                </div>
                <small>Total: @{{agencySelected.membership.agency_agent}}</small>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="body">
                <h3 class="number count-to m-b-0" data-from="0" :data-to="agencySelected.properties.length" data-speed="1000" data-fresh-interval="1">@{{agencySelected.properties.length}} <i class="zmdi zmdi-trending-up float-right"></i></h3>
                <p class="text-muted">@lang('custom/dashboard/index.properties')</p>
                <div class="progress">
                    <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="68" aria-valuemin="0" :aria-valuemax="agencySelected.membership.properties" :style="'width: '+agencySelected.properties.length*100/agencySelected.membership.properties+'%;'"></div>
                </div>
                <small>Total: @{{agencySelected.membership.properties}}</small>
            </div>
        </div>
    </div>
</div>
</div>
@endsection