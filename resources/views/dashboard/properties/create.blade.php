@extends('layouts.dashboard.app')
@php
    $lang = json_encode(__('custom/dashboard/properties.create'))
@endphp
@section('content')
<div class="row clearfix">  
        <dashboard-agency-properties-create :lang="{{$lang}}"></dashboard-agency-properties-create>
</div>
@endsection