@extends('layouts.errors.app')

@section('title', __('Maintenance work'))
@section('code', 'Maintenance Service')
@section('message', __('Maintenance work'))
