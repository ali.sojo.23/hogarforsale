<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;
    protected $fillable = [
        'agency_id',
        'title',
        'description',
        'feature_image',
        'country',
        'city',
        'postal_code',
        'address_line_1',
        'address_line_2',
        'latitude',
        'altitude',
        'gallery',
    ];
    protected $hidden = [
        'agency_id'
    ];
    public function agency()
    {
        return $this->hasOne(Agency::class, 'id', 'agency_id');
    }
}
