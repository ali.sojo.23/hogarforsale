<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgencyAgent extends Model
{
    use HasFactory;
    protected $fillable = [
        "user_id",
        "agency_id"
    ];
    protected $hidden = [
        "user_id",
        "agency_id",

        "updated_at",
    ];
    public function user()
    {
        return $this->hasOne(User::class, 'id', "user_id");
    }
    public function agencies()
    {
        return $this->hasOne(Agency::class, 'id', "agency_id");
    }
}
