<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Memberships extends Model
{
    use HasFactory;
    protected $fillable = [
        'feature_image',
        'title',
        'description',
        'price',
        'country',
        'properties',
        'admin_agency',
        'agency_agent',
        "type",
    ];
}
