<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'role',
        'email',
        'password',
        'feature_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function info()
    {
        return $this->hasOne('App\Models\info_user', 'user_id', 'id');
    }
    public function howAgent()
    {
        return $this->hasMany(AgencyAgent::class, 'user_id');
    }
    public function howAdmin()
    {
        return $this->hasMany(AgencyAdmin::class, 'user_id');
    }
    public function agencies()
    {
        return $this->howAdmin()->union($this->howAgent())->get();
    }
    public function isAgencyAdmin($id)
    {
        return $this->howAdmin()->where('agency_id', $id)->get();
    }
    public function preferences()
    {
        return $this->hasOne(UserPreference::class, 'user_id');
    }
}
