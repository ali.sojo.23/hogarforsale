<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    use HasFactory;
    protected $fillable = [
        "user_id",
        "company_name",
        "comercial_name",
        "rif",
        "phone",
        "mobile",
        "country",
        "city",
        "postal_code",
        "logo",
        'facebook',
        'twitter',
        'linkedin',
        'instagram',
        'description',
        "membership_id",
    ];
    protected $hidden = [
        "membership_id"
    ];
    public function admins()
    {
        return $this->hasMany(AgencyAdmin::class, 'agency_id');
    }
    public function agents()
    {
        return $this->hasMany(AgencyAgent::class, 'agency_id');
    }
    public function GetAllFunctions($id)
    {
        return $this->admins()->union($this->agents())->where('user_id', $id)->get();
    }
    public function membership()
    {
        return $this->hasOne(Memberships::class, 'id', "membership_id");
    }
    public function properties()
    {
        return $this->hasMany(Property::class, 'agency_id');
    }
}
