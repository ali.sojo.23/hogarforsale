<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Client\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($request, $response)
    {

        if ($request->wantsJson()) {
            return new JsonResponse(['message' => trans($response)], 200);
        }
        $token = $request->user()->createToken($request->_token);
        Cookie::queue(Cookie::make('auth_token', $token->plainTextToken, 180, null, null, false, false));
        $this->middleware('set.preference.config');
        if ($request->user()->hasRole(['admin', 'agency.admin', 'agency.agent', 'agent'])) :
            return redirect()->route('dashboard.index')->with('status', trans($response));
        else :
            return redirect()->route('profile.profile')->with('status', trans($response));
        endif;
    }
}
