<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Memberships;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MembershipsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $m = null;
        if ($request['withCountry']) :
            $m = Memberships::where('country', $request['withCountry']);
        elseif ($request['withType']) :
            $m = Memberships::where('type', $request['withType']);
        endif;
        if ($m && $request['orderBy']) :
            $m->orderBy($request['orderBy'], $request['orderMode']);
        endif;
        return $m->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = asset('img/logo.png');
        $country = $request['country'] ?? 'ALL';
        if ($request['feature_image']) :
            $file = str_replace(env('APP_URL') . '/storage/', '', $request['feature_image']);
            $path = str_replace('image/', '', $file);

            $file = Storage::disk('public')->move($file, 'user/' . $path);
            if ($file == true) {
                $file = asset('storage/user/' . $path);
            }
        endif;
        $m = Memberships::create([
            'feature_image' => $file,
            'title' => $request['title'],
            'description' => $request['description'],
            'price' => $request['price'],
            'country' => $country,
            'properties' => $request['properties'],
            'admin_agency' => $request['admin_agency'],
            'agency_agent' => $request['agency_agent'],
            'type' => $request['type'][0],
        ]);
        return $m;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function show(Memberships $memberships)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function edit(Memberships $memberships)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Memberships $memberships)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function destroy(Memberships $memberships)
    {
        //
    }
}
