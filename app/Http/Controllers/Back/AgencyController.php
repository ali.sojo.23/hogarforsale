<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Mail\AssignedUserToAgency;
use App\Models\Agency;
use App\Models\AgencyAdmin;
use App\Models\Memberships;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AgencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request['country']) :
            $a = Agency::where('country', $request['country'])->get();
        elseif ($request['userHasRole'] === 'admin') :
            $a = Agency::with(['admins', 'agents', 'membership', 'properties'])->get();
        elseif ($request['assignedToUser']) :
            $s = $request['assignedToUser'];
            $a = Agency::whereHas(
                'admins.user',
                function ($q) use ($s) {
                    $q->where('id', $s);
                },
            )->orWhereHas(
                'agents.user',
                function ($q) use ($s) {
                    $q->where('id', $s);
                }
            )->with(['agents.user', 'admins.user', 'membership', 'properties'])->get();
        endif;
        return $a ?? abort(404);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = asset('img/logo.png');
        if ($request['user_id']) {
            $user = User::find($request['user_id']);
            if ($user) {
                $user->givePermissionTo('agency.admin');
                $user->save();
                if ($request['feature_image']) {
                    $file = str_replace(env('APP_URL') . '/storage/', '', $request['feature_image']);
                    $path = str_replace('temp/', '', $file);

                    $file = Storage::disk('public')->move($file, 'agency/' . $path);
                    if ($file == true) {
                        $file = asset('storage/agency/' . $path);
                    }
                }
                $a = Agency::create([
                    "user_id" => $request["user_id"],
                    "company_name" => $request["company_name"],
                    "comercial_name" => $request["comercial_name"],
                    "rif" => $request["rif"],
                    "phone" => $request["phone"],
                    "mobile" => $request["mobile"],
                    "country" => $request["country"],
                    "city" => $request["city"],
                    "postal_code" => $request["postal_code"],
                    "logo" => $file,
                ]);
                AgencyAdmin::create([
                    "user_id" => $user->id,
                    "agency_id" => $a->id
                ]);
                Mail::to($user->email)->send(new AssignedUserToAgency($user));
                return $a;
            } else {
                return response(['msg' => __('custom/dashboard/agency.messages.error.not_user')]);
            }
        } else {
            return response(['msg' => __('custom/dashboard/agency.messages.error.not_user')]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function show(Agency $agency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agency $agency)
    {

        if (!$agency) {
            return abort(400);
        }
        if ($request['setMembership'] && $request['membership']) :
            if ($request['setMembership'] == $request['membership']['id']) :
                $m = Memberships::findOrFail($request['membership']['id']);
                if ($m) :
                    if ($m->country == 'ALL') :
                        if ($m->type == 'all' || $m->type == 'agency') :
                            $agency->membership_id = $m->id;
                            $agency->save();
                            return $agency;
                        else :
                            return response(['msg' => __('Unauthorized')], 403);
                        endif;
                    elseif ($m->country == $agency->country) :
                        if ($m->type == 'all' || $m->type == 'agency') :
                            $agency->membership_id = $m->id;
                            $agency->save();
                            return $agency;
                        else :
                            return response(['msg' => __('Unauthorized')], 403);
                        endif;
                    else :
                        return response(['msg' => __('Unauthorized')], 403);
                    endif;
                endif;
            else :
                return response(['msg' => __('Unauthorized')], 403);
            endif;
        endif;
        if ($request['logo'] != $agency->logo) {
            $file = str_replace(env('APP_URL') . '/storage/', '', $request['logo']);
            $path = str_replace('temp/', '', $file);

            $file = Storage::disk('public')->move($file, 'agency/' . $path);
            if ($file == true) {
                $file = asset('storage/agency/' . $path);
            }

            $agency->logo = $file;
        }
        $agency->facebook = $request['facebook'];
        $agency->twitter = $request['twitter'];
        $agency->linkedin = $request['linkedin'];
        $agency->instagram = $request['instagram'];
        $agency->description = $request['description'];
        $agency->save();
        return response(['agency' => $agency, "msg" => __('custom/dashboard/agency.show.message.success.updated')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agency $agency)
    {
        //
    }
}
