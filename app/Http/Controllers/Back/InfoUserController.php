<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\info_user;
use App\Models\User;
use Illuminate\Http\Request;

class InfoUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $u = User::find($id);
        if (!$u) {
            return response(['message' => __('custom/dashboard/users.profile.response.error.user_not_found')]);
        }
        $u = info_user::where('user_id', $id)->first();
        $i = $request->info;
        if (!$u) {
            $info = info_user::create([
                'user_id' => $id,
                'country' => $i['country'],
                'ciudad' => $i['ciudad'],
                'address_1' => $i['address_1'],
                'address_2' => $i['address_2'],
                'phone' => $i['phone'],
                'description' => $i['description'],
                'postal_code' => $i['postal_code'],
                'dni' => $i['dni'],
                'gender' => $i['gender'],
                'birthdate' => $i['birthdate']
            ]);
            return response(['message' => __('custom/dashboard/users.profile.response.success.profile_updated')]);
        } else {
            $u->country = $i['country'];
            $u->ciudad = $i['ciudad'];
            $u->address_1 = $i['address_1'];
            $u->address_2 = $i['address_2'];
            $u->phone = $i['phone'];
            $u->description = $i['description'];
            $u->postal_code = $i['postal_code'];
            $u->dni = $i['dni'];
            $u->gender = $i['gender'];
            $u->birthdate = $i['birthdate'];
            $u->save();

            return response(['message' => __('custom/dashboard/users.profile.response.success.profile_updated')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
