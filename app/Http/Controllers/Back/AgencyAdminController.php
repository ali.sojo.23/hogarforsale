<?php

namespace App\Http\Controllers;

use App\Models\AgencyAdmin;
use Illuminate\Http\Request;

class AgencyAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AgencyAdmin  $agencyAdmin
     * @return \Illuminate\Http\Response
     */
    public function show(AgencyAdmin $agencyAdmin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AgencyAdmin  $agencyAdmin
     * @return \Illuminate\Http\Response
     */
    public function edit(AgencyAdmin $agencyAdmin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AgencyAdmin  $agencyAdmin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgencyAdmin $agencyAdmin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AgencyAdmin  $agencyAdmin
     * @return \Illuminate\Http\Response
     */
    public function destroy(AgencyAdmin $agencyAdmin)
    {
        //
    }
}
