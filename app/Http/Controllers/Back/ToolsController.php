<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\UserPreference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class ToolsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function setLang($lang)
    {
        $t = UserPreference::where('user_id', Auth::id())->first();
        if ($t) :
            $t->lang = $lang;
            $t->save();
        else :
            UserPreference::create([
                'user_id' => Auth::id(),
                'lang' => $lang
            ]);
        endif;
        Cookie::queue(Cookie::make('lang', $lang, 1440));
        return redirect()->back();
    }
    public function setTemplate($template)
    {
        $t = UserPreference::where('user_id', Auth::id())->first();
        if ($t) :
            $t->template = $template;
            $t->save();
        else :
            UserPreference::create([
                'user_id' => Auth::id(),
                'template' => $template
            ]);
        endif;
        Cookie::queue(Cookie::make('template', $template, 1440));
        return redirect()->back();
    }
    public function setMenuTemplate($template)
    {
        $t = UserPreference::where('user_id', Auth::id())->first();
        if ($t) :
            $t->left_sidebar_template = $template;
            $t->save();
        else :
            UserPreference::create([
                'user_id' => Auth::id(),
                'left_sidebar_template' => $template
            ]);
        endif;
        Cookie::queue(Cookie::make('menu_template', $template, 1440));
        return redirect()->back();
    }
    public function setTimezone($timezone)
    {
        $t = UserPreference::where('user_id', Auth::id())->first();
        if ($t) :
            $t->time_zone = $timezone;
            $t->save();
        else :
            UserPreference::create([
                'user_id' => Auth::id(),
                'time_zone' => $timezone
            ]);
        endif;
        Cookie::queue(Cookie::make('timezone', $timezone, 1440));
        return redirect()->back();
    }
    public function extendSession(Request $request)
    {
        $token = $request->user()->createToken($request->_token);
        Cookie::queue(Cookie::make('auth_token', $token->plainTextToken, 180, null, null, false, false));
        return back();
    }
}
