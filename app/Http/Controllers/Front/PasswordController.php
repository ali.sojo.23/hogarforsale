<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($token, request $r)
    {
        $pr = PasswordReset::where(['token' => $token, 'email' => $r->email])
            ->firstOrFail();

        return view('auth.passwords.create', [
            "token" => $pr->token,
            "email" => $pr->email
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                'exists:password_resets,email',
                'exists:users,email'
            ],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'token' => ['required', 'exists:password_resets,token']
        ]);
        if ($validator->fails()) :
            return back()->withErrors($validator);
        endif;
        $u = User::where('email', $request->email)->first();
        $u->password = Hash::make($request->password);
        $u->save();

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('dashboard');
        }
    }
}
