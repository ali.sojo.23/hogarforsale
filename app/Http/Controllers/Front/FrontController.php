<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index(){
        return view('frontend.home',[
            "styles" => [
                "lib/swiper/css/swiper.min.css",
            ],
            "scripts" => [
            ],
            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=".env('MIX_GOOGLE_MAPS_API_KEY')."&libraries=places"
        ]);
    }
    public function results(Request $request){
        if($request->routeIs('sell.*')){
            $type = "sell";
        }
        if($request->routeIs('rent.*')){
            $type = "rent";
        }
        // return $type;
        // return $request->all();
        return view('frontend.list',[
            "styles" => [
                
            ],
            "type" => $type
        ]);
    }
    public function details($hfsid, Request $request){
        if($request->routeIs('sell.*')){
            $type = "sell";
        }
        if($request->routeIs('rent.*')){
            $type = "rent";
        }
        // return [$type,$hfsid];
        return view('frontend.details',[
        "styles" => [
            "lib/swiper/css/swiper.min.css",
        ]
        ]);
    }
}
