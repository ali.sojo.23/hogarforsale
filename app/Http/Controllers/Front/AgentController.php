<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        return view('frontend.profile.profile', [
            "styles" => [],
        ]);
    }
    public function password()
    {
        return view('frontend.profile.password', [
            "styles" => []
        ]);
    }
    public function notifications()
    {
        return view('frontend.profile.notifications', [
            "styles" => []
        ]);
    }
    public function memberships()
    {
        return view('frontend.profile.memberships', [
            "styles" => []
        ]);
    }
}
