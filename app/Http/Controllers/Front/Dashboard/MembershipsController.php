<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Memberships;
use Illuminate\Http\Request;

class MembershipsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $m = Memberships::groupBy('type')->pluck('type');

        $b = [
            __('custom/dashboard/memberships.breadcrumb.memberships')
        ];
        $t = __('custom/dashboard/memberships.title');
        $st = __('custom/dashboard/memberships.index.subtitle');
        return view('dashboard.memberships.index', [
            "breadcrumb" => $b,
            "title" => $t,
            "memberships" => $m,
            "subtitle" => $st,
            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=" . env('MIX_GOOGLE_MAPS_API_KEY') . "&libraries=places"
        ]);
    }
    public function create()
    {
        $b = [
            __('custom/dashboard/memberships.breadcrumb.memberships'),
            __('custom/dashboard/memberships.breadcrumb.create')
        ];
        $t = __('custom/dashboard/index.title');
        $st = __('custom/dashboard/index.subtitle');
        return view('dashboard.memberships.create', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=" . env('MIX_GOOGLE_MAPS_API_KEY') . "&libraries=places"
        ]);
    }
}
