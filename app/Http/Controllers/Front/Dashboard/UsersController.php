<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $css = Cookie::get('template');
        if ($css === 'dark') {
            $this->css = 'css/dashboard/dark/main.css';
        } else {
            $this->css = 'css/dashboard/light/main.css';
        }
    }
    public function index()
    {
        // $u = User::all()->groupBy('role');
        $u = Role::all()->pluck('name');
        $b = [
            __('custom/dashboard/users.breadcrumb.users'),
            __('custom/dashboard/users.breadcrumb.admin')
        ];
        $t = __('custom/dashboard/users.title');
        $st = __('custom/dashboard/users.subtitle');

        return view('dashboard.users.datatable', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "users" => $u,
            'css' => $this->css
        ]);
    }
    public function show(User $id)
    {
        $b = [
            __('custom/dashboard/users.breadcrumb.users'),
            __('custom/dashboard/users.breadcrumb.admin')
        ];
        $t = __('custom/dashboard/users.title');
        $st = __('custom/dashboard/users.subtitle');

        return view('dashboard.users.show', [
            "user" => $id,
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            'css' => $this->css,
            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=" . env('MIX_GOOGLE_MAPS_API_KEY') . "&libraries=places"
        ]);
    }
}
