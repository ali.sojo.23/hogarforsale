<?php

namespace App\Http\Controllers\Front\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Agency;
use App\Models\AgencyAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class AgencyController extends Controller
{
    public $css;
    public function __construct()
    {
        $this->middleware('auth');
        $css = Cookie::get('template');
        if ($css == 'dark') {
            $this->css = 'css/dashboard/dark/main.css';
        } else {
            $this->css = 'css/dashboard/light/main.css';
        }
    }
    public function index()
    {
        $a = Agency::all('country')->groupBy('country');
        $b = [
            __('custom/dashboard/agency.breadcrumb.agencies'),
            __('custom/dashboard/agency.breadcrumb.list')
        ];
        $t = __('custom/dashboard/agency.title');
        $st = __('custom/dashboard/agency.create.subtitle');

        return view('dashboard.agency.index', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "agency" => $a,
            'css' => $this->css
        ]);
    }
    public function create()
    {

        $b = [
            __('custom/dashboard/agency.breadcrumb.agencies'),
            __('custom/dashboard/agency.breadcrumb.create')
        ];
        $t = __('custom/dashboard/agency.title');
        $st = __('custom/dashboard/agency.create.subtitle');

        return view('dashboard.agency.create', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            'css' => $this->css,
            "googleApi" => "https://maps.googleapis.com/maps/api/js?key=" . env('MIX_GOOGLE_MAPS_API_KEY') . "&libraries=places"
        ]);
    }
    public function show(Request $request, $id)
    {
        $user = $request->user()->id;
        if ($request->user()->hasRole('admin')) :
            $a = Agency::where('id', $id)->with(['admins', 'agents', 'properties'])->first();
        else :
            $a = Agency::whereHas('admins', function ($q) use ($user) {
                $q->where('user_id', $user);
            })->orWhereHas('agents', function ($q) use ($user) {
                $q->where('user_id', $user);
            })->with(['admins', 'agents', 'properties'])->get()->where('id', $id)->first();
        endif;
        if (!$a) {
            return abort(401);
        }
        $b = [
            __('custom/dashboard/agency.breadcrumb.agencies'),
            __('custom/dashboard/agency.breadcrumb.create')
        ];
        $t = __('custom/dashboard/agency.title');
        $st = __('custom/dashboard/agency.index.subtitle');

        return view('dashboard.agency.show', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "agency" => $a,
            'css' => $this->css,
        ]);
    }
    public function admin(Request $request)
    {
        if ($request->user()->hasRole('admin')) :
            $a = Agency::with(['admins', 'agents'])->get();
        elseif ($request->routeIs('dashboard.agency.list.admin')) :
            $a = Agency::whereHas('admins.user', function ($q) {
                $q->where('id', Auth::user()->id);
            })->with(['admins', 'agents', 'properties'])->get();

        endif;

        $b = [
            __('custom/dashboard/agency.breadcrumb.agencies'),
            __('custom/dashboard/agency.breadcrumb.set_admin')
        ];
        $t = __('custom/dashboard/agency.title');
        $st = __('custom/dashboard/agency.list.subtitle_admin');

        return view('dashboard.agency.list.admin', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "agency" => $a,
            'css' => $this->css,
        ]);
    }
    public function agent(Request $request)
    {
        if ($request->user()->hasRole('admin')) :
            $a = Agency::with(['admins', 'agents'])->get();
        elseif ($request->routeIs('dashboard.agency.list.agent')) :
            $a = Agency::whereHas('admins.user', function ($q) {
                $q->where('id', Auth::user()->id);
            })->with(['admins', 'agents', 'properties'])->get();

        endif;

        $b = [
            __('custom/dashboard/agency.breadcrumb.agencies'),
            __('custom/dashboard/agency.breadcrumb.set_agent')
        ];
        $t = __('custom/dashboard/agency.title');
        $st = __('custom/dashboard/agency.list.subtitle_agent');

        return view('dashboard.agency.list.agent', [
            "breadcrumb" => $b,
            "title" => $t,
            "subtitle" => $st,
            "agency" => $a,
            'css' => $this->css,
        ]);
    }
}
