<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class SetCookiesPreferences
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if (Auth::check()) :

            Cookie::queue(Cookie::make('template', Auth::user()->preferences['template'] ?? Cookie::queue(Cookie::make('template', 'light', 1440)), 1440));
            Cookie::queue(Cookie::make('menu_template', Auth::user()->preferences['left_sidebar_template'] ?? Cookie::queue(Cookie::make('menu_template', 'light', 1440)), 1440));
            Cookie::queue(Cookie::make('currency', Auth::user()->preferences['currency'] ?? Cookie::queue(Cookie::make('currency', 'USD', 1440)), 1440));

        else :
            Cookie::get('template') ?? Cookie::queue(Cookie::make('template', 'light', 1440));
            Cookie::get('menu_template') ?? Cookie::queue(Cookie::make('menu_template', 'light', 1440));
            Cookie::get('currency') ?? Cookie::queue(Cookie::make('currency', 'USD', 1440));
        endif;
        return $next($request);
    }
}
