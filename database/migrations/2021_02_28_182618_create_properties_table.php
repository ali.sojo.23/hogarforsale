<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('agency_id')->unsigned();
            $table->string('title');
            $table->longText('description');
            $table->string('property_type');
            $table->string('ofert_type');
            $table->string('feature_image');
            $table->string('country');
            $table->string('city');
            $table->string('postal_code');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->float('latitude');
            $table->float('altitude');
            $table->longText('gallery');
            $table->timestamps();

            $table->foreign('agency_id')
                ->references('id')->on('agencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
