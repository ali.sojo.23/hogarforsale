<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(['name' => 'admin']);
        $admin->givePermissionTo(Permission::all());
        $guest = Role::create(['name' => "guest"]);
        $guest->givePermissionTo('guest');
        $agency = Role::create(['name' => "agency.admin"]);
        $agency->givePermissionTo('agency.admin');
        $agencyAgent = Role::create(['name' => "agency.agent"]);
        $agencyAgent->givePermissionTo('agency.agent');
        $agent = Role::create(['name' => "agent"]);
        $agent->givePermissionTo('agent');
        
        $user = User::find(1);
        $user->assignRole('admin');

        $user = User::find(2);
        $user->assignRole('admin');
        
    }
}
