<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'agency.admin']);
        Permission::create(['name' => 'agency.agent']);
        Permission::create(['name' => 'agent']);
        Permission::create(['name' => 'guest']);
    }
}
