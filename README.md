<p align="center"><a href="https://hogarforsale.com" target="_blank"><img src="http://hogarforsale.com/assets/img/logo.png" width="400"></a></p>
</p>

## About Hogar For Sale


## Fundadores


## Requeriments

* PHP >=7
* COMPOSER
* NPM

## Instalación

1. `git clone git@gitlab.com:ali.sojo.23/hogarforsale.git`
2. `cp .env.example .env`
3. `composer install && npm install`
4. `php artisan key:gen`
5. `nano .env` and edit DB, APP_URL, IF Prod => APP_DEBUG && APP_ENV && APP_NAME and other fields; ELSE only replace necesary fields
6. `php artisan migrate --seed`
7. IF Prod `npm run watch` || `npm run dev` ELSE `npm run prod`

### Paquetes Composer
- **[laravel-pt-BR-localization](https://github.com/lucascudo/laravel-pt-BR-localization)**
- **[Laraveles/spanish](https://github.com/Laraveles/spanish)**
- **[spatie/laravel-permission](https://spatie.be/docs/laravel-permission/v3/introduction)**


### Paquetes NPM
- **[marker-clusterer-plus](https://www.npmjs.com/package/marker-clusterer-plus)**
- **[swiper](https://swiperjs.com/vue/)**
- **[vue-awesome-swiper](https://www.npmjs.com/package/vue-awesome-swiper)**
- **[vue-lazyload](https://www.npmjs.com/package/vue-lazyload)**
- **[vue-photoswipe.js](https://github.com/tinymins/vue-photoswipe.js)**
- **[vue-select](https://vue-select.org/)**
- **[vue-sticky-sidebar](https://www.npmjs.com/package/vue-sticky-sidebar)**
- **[vue-google-autocomplete](https://www.npmjs.com/package/vue-google-autocomplete)**
- **[vue2-dropzone](https://rowanwins.github.io/vue-dropzone/docs/dist/#/installation)**
- **[vue2-google-maps](https://www.npmjs.com/package/vue2-google-maps)**
- **[vulidate](https://vuelidate.js.org/#getting-started)**
- **[vue-form-wizard](https://binarcode.github.io/vue-form-wizard/#/)**
- **[vue-phone-number-input](https://www.npmjs.com/package/vue-phone-number-input)**

## Externals API
- **[exchangerate-api](https://app.exchangerate-api.com/)**
- **[blockchain api | BTC](https://blockchain.info/ticker)**

## Maintenance Mode
Into this mode
1. `php artisan down --render=errors.down --secret=we-are-working-to-best-work`

## Version Controller

* Version 0.2.0

Es una versión beta, pre produción, se muestra:
    1. Administración de Usuarios
    2. Administracion de agencias
        1. Usuarios administrativos

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
