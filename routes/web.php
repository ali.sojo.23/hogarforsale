<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['namespace' => 'App\Http\Controllers\Front'], function () {
    Route::get('/', 'FrontController@index')->name('index');
    Route::group(['prefix' => "auth"], function () {
        ROute::get('password/create/{token}', 'PasswordController@index')->name('auth.password.create');
        ROute::post('password/create/', 'PasswordController@update')->name('auth.password.update');
    });
    Route::group(['prefix' => "sell"], function () {
        Route::get('/', 'FrontController@results')->name('sell.results');
        Route::get('/propertydetails/{hfsid}', 'FrontController@details')->name('sell.details');
    });
    Route::group(['prefix' => "rent"], function () {
        Route::get('/', 'FrontController@results')->name('rent.results');
        Route::get('/propertydetails/{hfsid}', 'FrontController@details')->name('rent.details');
    });
    Route::group(['prefix' => "profile"], function () {
        Route::get('/', 'AgentController@profile')->name('profile.profile');
        Route::get('password', 'AgentController@password')->name('profile.password');
        Route::get('notifications', 'AgentController@notifications')->name('profile.notifications');
        Route::get('memberships', 'AgentController@memberships')->name('profile.memberships');
    });
    Route::group([
        'prefix' => "dashboard",
        "middleware" => ["permission:admin|agency.admin|agency.agent|agent"],
        "namespace" => "Dashboard"
    ], function () {
        Route::get('/', 'IndexController@index')->name('dashboard.index');
        Route::get('my-profile', 'IndexController@profile')->name('dashboard.profile');

        Route::group(['prefix' => "users", "middleware" => "role:admin"], function () {
            Route::get('/', 'UsersController@index')->name('dashboard.users');
            Route::get('{id}', 'UsersController@show')->name('dashboard.users.show');
        });
        Route::group(["prefix" => "agency"], function () {
            Route::get('/', 'AgencyController@index')->name('dashboard.agency.index')->middleware('role:admin');
            Route::get('create', 'AgencyController@create')->name('dashboard.agency.create')->middleware('role:admin');
            Route::get('/show/{id}', 'AgencyController@show')->name('dashboard.agency.show')->middleware('permission:admin|agency.admin|agency.agent');
            Route::get('list/admin', 'AgencyController@admin')->name('dashboard.agency.list.admin')->middleware('permission:admin|agency.admin');
            Route::get('list/agent', 'AgencyController@agent')->name('dashboard.agency.list.agent')->middleware('permission:admin|agency.admin|agency.agent');
            Route::group(['prefix' => 'properties', "middleware" => "permission:admin|agency.admin|agency.agent"], function () {
                Route::get('create', 'PropertiesController@create')->name('dashboard.agency.properties.create');
                Route::get('ofert-type/{ofert}', 'PropertiesController@index')->name('dashboard.agency.properties.index');
            });
            Route::get('memberships', 'MembershipsController@index')->name('dashboard.agency.memberships.index')->middleware('role:admin');
            Route::get('memberships/create', 'MembershipsController@create')->name('dashboard.agency.memberships.create')->middleware('role:admin');
        });
    });
});
Route::group(["namespace" => 'App\Http\Controllers\Back'], function () {
    Route::get('lang/{lang}', 'ToolsController@setLang')->name('tools.set.lang');
    Route::get('template/{template}', 'ToolsController@setTemplate')->name('tools.set.template');
    Route::get('menu-template/{template}', 'ToolsController@setMenuTemplate')->name('tools.set.menu_template');
    Route::get('timezone/{timezone}', 'ToolsController@setTimezone')->name('tools.set.timezone');
    Route::post('extend-session', 'ToolsController@extendSession')->name('extend.sessions');
});
